package com.desa02.tareafragments.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.desa02.tareafragments.R
import com.desa02.tareafragments.databinding.ItemContactoBinding
import com.desa02.tareafragments.model.Contacto
import com.squareup.picasso.Picasso

class ContactoAdapter(var contacto: MutableList<Contacto> = mutableListOf(), val callback: (item: Contacto)-> Unit)
    : RecyclerView.Adapter<ContactoAdapter.ContactoAdapterViewHolder>(){

    inner class ContactoAdapterViewHolder(itemView: View): RecyclerView.ViewHolder( itemView ){
        private val binding: ItemContactoBinding = ItemContactoBinding.bind(itemView)
        fun bin(contacto:Contacto){
            binding.tvNombreContacto.text = contacto.nombre
            Picasso.get().load(contacto.photo).into(binding.imvContacto)
            itemView.setOnClickListener {
                callback(contacto)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactoAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_contacto,parent,false)
        return ContactoAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
       return contacto.size
    }

    override fun onBindViewHolder(holder: ContactoAdapterViewHolder, position: Int) {
        val contacto : Contacto = contacto[position]
        holder.bin( contacto )
    }
}