package com.desa02.tareafragments.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.desa02.tareafragments.R
import com.desa02.tareafragments.databinding.ItemProductoBinding
import com.desa02.tareafragments.model.Producto
import com.squareup.picasso.Picasso

class ProductoAdapter( var producto:MutableList<Producto> = mutableListOf(), val callback: (item:Producto)-> Unit )
    : RecyclerView.Adapter<ProductoAdapter.ProductoAdapterViewHolder>() {

    inner class ProductoAdapterViewHolder(itemView: View): RecyclerView.ViewHolder( itemView ){

        private val binding:ItemProductoBinding = ItemProductoBinding.bind(itemView)

        fun bin(producto:  Producto ){
            binding.tvPrecio.text = producto.precio
            binding.tvProducto.text = producto.nombre
            Picasso.get().load(producto.photo).into(binding.imvProducto)
            itemView.setOnClickListener {
                callback(producto)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductoAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_producto,parent,false)
        return ProductoAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return producto.size
    }

    override fun onBindViewHolder(holder: ProductoAdapterViewHolder, position: Int) {
        val producto : Producto = producto[position]
        holder.bin( producto )
    }

    fun updateProducto( productos: MutableList<Producto> = mutableListOf() ){
        this.producto = productos
        notifyDataSetChanged()
    }
}