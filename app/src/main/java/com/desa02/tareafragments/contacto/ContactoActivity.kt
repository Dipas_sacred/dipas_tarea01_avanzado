package com.desa02.tareafragments.contacto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.desa02.tareafragments.R
import com.desa02.tareafragments.databinding.ActivityContactoBinding
import com.desa02.tareafragments.model.Contacto
import com.desa02.tareafragments.producto.DetalleProductoFragment
import com.desa02.tareafragments.producto.ProductoFragment

class ContactoActivity : AppCompatActivity(), OnContactoListener {

    lateinit var binding: ActivityContactoBinding
    private lateinit var contactoFragment: ContactoFragment
    private lateinit var contactoDetalleFragment: ContactoDetalleFragment
    private lateinit var fragmentManager: FragmentManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityContactoBinding.inflate( layoutInflater )
        setContentView( binding.root )

        fragmentManager= supportFragmentManager
        contactoFragment= fragmentManager.findFragmentById(R.id.fragContacto) as ContactoFragment
        contactoDetalleFragment= fragmentManager.findFragmentById(R.id.fragContactoDetalle) as ContactoDetalleFragment
    }

    override fun onSendMessage(msg: String) {
        TODO("Not yet implemented")
    }

    override fun selectedItemContact(contacto: Contacto) {
        contactoDetalleFragment.renderContact( contacto )
    }

    override fun renderFirst(contacto: Contacto?) {
        contacto?.let {
            selectedItemContact(it)
        }
    }
}