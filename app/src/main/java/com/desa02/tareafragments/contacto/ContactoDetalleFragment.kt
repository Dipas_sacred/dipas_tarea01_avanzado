package com.desa02.tareafragments.contacto

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.desa02.tareafragments.R
import com.desa02.tareafragments.model.Contacto
import com.desa02.tareafragments.model.Producto
import com.desa02.tareafragments.producto.OnProductoListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_contacto_detalle.*
import kotlinx.android.synthetic.main.fragment_detalle_producto.*
import kotlinx.android.synthetic.main.item_contacto.*
import kotlinx.android.synthetic.main.item_contacto.tv_nombre_contacto
import kotlinx.android.synthetic.main.item_producto.*
import kotlinx.android.synthetic.main.item_producto.tv_precio
import kotlinx.android.synthetic.main.item_producto.tv_producto

class ContactoDetalleFragment : Fragment() {

    private var listener : OnContactoListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contacto_detalle, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnContactoListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnContactListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    fun renderContact(contacto: Contacto) {
        tv_nombre_contacto.text = contacto.nombre
        tv_email_contacto.text = contacto.email
        tv_web_contacto.text = contacto.website
        tv_telefono_contacto.text = contacto.telefono
        Picasso.get().load( contacto.photo ).into( imv_contacto_detalle )
    }

}