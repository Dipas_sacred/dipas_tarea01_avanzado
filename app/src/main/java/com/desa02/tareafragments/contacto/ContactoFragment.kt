package com.desa02.tareafragments.contacto

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.desa02.tareafragments.R
import com.desa02.tareafragments.adapters.ContactoAdapter
import com.desa02.tareafragments.model.Contacto
import kotlinx.android.synthetic.main.fragment_contacto.*
import kotlinx.android.synthetic.main.fragment_producto.*

class ContactoFragment : Fragment() {

    private var contacto = mutableListOf<Contacto>()
    private var listener :OnContactoListener? = null
    private val adaptador by lazy {
        ContactoAdapter( contacto ){ cred ->
            listener?.let { itemSeleccionado ->
                itemSeleccionado.selectedItemContact( cred )
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contacto, container, false)
    }

    private fun setData() {
        contacto.add(
            Contacto(1,"Juan Perez","998765488","juan@email.com","www.jose.com"
            ,"https://orientacion-laboral.infojobs.net/wp-content/uploads/2017/11/9-si-foto-cv.png")
        )

        contacto.add(
            Contacto(2,"Roberto Lopez","976511233","roberto@email.com","www.roberto.com"
                ,"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQqIRWnY-4RAjmnUcNDqHRFtiimqcAJI0KyQ&usqp=CAU")
        )

        contacto.add(
            Contacto(3,"Maria Perez","998445488","maria@email.com","www.maria.com"
                ,"https://www.superprof.com.ar/imagenes/avisos/profesor-home-revision-curriculum-evaluacion-perfil-ayudo-buscar-proximo-empleo.jpg")
        )

    }

    private fun first(): Contacto? {
        contacto?.let {
            return it[0]
        }
        return null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setData()
        context?.let {
            rv_contactos.adapter = adaptador
            rv_contactos.layoutManager = LinearLayoutManager(it)
        }

        listener?.renderFirst(first())
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnContactoListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnContactoListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}