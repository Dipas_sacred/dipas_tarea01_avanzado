package com.desa02.tareafragments.contacto

import com.desa02.tareafragments.model.Contacto

interface OnContactoListener {
    fun onSendMessage(msg: String)
    fun selectedItemContact(contacto: Contacto )
    fun renderFirst(contacto: Contacto?)
}