package com.desa02.tareafragments.model

import java.io.Serializable

data class Contacto(val id:Int,val nombre:String, val telefono:String, val email:String, val website:String,
val photo:String)
    : Serializable{

}