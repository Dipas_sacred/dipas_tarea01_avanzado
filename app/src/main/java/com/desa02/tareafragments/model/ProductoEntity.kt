package com.desa02.tareafragments.model

import java.io.Serializable

data class Producto(val id:Int,val nombre:String,val precio:String,
                          val photo:String,val descripcion:String): Serializable {
}