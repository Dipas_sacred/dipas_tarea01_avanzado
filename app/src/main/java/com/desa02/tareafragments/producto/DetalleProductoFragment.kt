package com.desa02.tareafragments.producto

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.desa02.tareafragments.R
import com.desa02.tareafragments.adapters.ProductoAdapter
import com.desa02.tareafragments.model.Producto
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_detalle_producto.*
import kotlinx.android.synthetic.main.fragment_producto.*
import kotlinx.android.synthetic.main.item_producto.*
import kotlinx.android.synthetic.main.item_producto.tv_precio
import kotlinx.android.synthetic.main.item_producto.tv_producto

class DetalleProductoFragment: Fragment() {

    private var listener :OnProductoListener?=null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detalle_producto, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnProductoListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnContactListener")
        }
    }

    fun renderContact(producto: Producto) {
        tv_producto.text = producto.nombre
        tv_precio.text = producto.precio
        tv_descripcion.text = producto.descripcion
        Picasso.get().load( producto.photo ).into( imv_producto_detalle )
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}