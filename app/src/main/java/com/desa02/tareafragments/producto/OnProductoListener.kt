package com.desa02.tareafragments.producto

import com.desa02.tareafragments.model.Producto

interface OnProductoListener {
    fun onSendMessage(msg: String)
    fun selectedItemContact(producto: Producto)
    fun renderFirst(producto: Producto?)
}