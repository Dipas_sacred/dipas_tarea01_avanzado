package com.desa02.tareafragments.producto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.desa02.tareafragments.R
import com.desa02.tareafragments.databinding.ActivityProductoBinding
import com.desa02.tareafragments.model.Producto

class ProductoActivity : AppCompatActivity(), OnProductoListener {

    lateinit var binding : ActivityProductoBinding
    private lateinit var productoFragment: ProductoFragment
    private lateinit var productoDetalleFragment: DetalleProductoFragment
    private lateinit var fragmentManager: FragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_producto)

        fragmentManager= supportFragmentManager
        productoFragment= fragmentManager.findFragmentById(R.id.fragProducto) as ProductoFragment
        productoDetalleFragment= fragmentManager.findFragmentById(R.id.fragProductoDetalle) as DetalleProductoFragment
    }

    override fun onSendMessage(msg: String) {
        TODO("Not yet implemented")
    }

    override fun selectedItemContact(producto: Producto) {
        productoDetalleFragment.renderContact( producto )
    }

    override fun renderFirst(producto: Producto?) {
        producto?.let {
            selectedItemContact(it)
        }
    }
}