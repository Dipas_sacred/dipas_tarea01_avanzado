package com.desa02.tareafragments.producto

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.desa02.tareafragments.R
import com.desa02.tareafragments.adapters.ProductoAdapter
import com.desa02.tareafragments.model.Producto
import kotlinx.android.synthetic.main.fragment_producto.*

class ProductoFragment: Fragment() {

    private var listener :OnProductoListener?=null
    private var productos = mutableListOf<Producto>()
    private val adaptador by lazy {
        ProductoAdapter( productos ){ prod ->
            listener?.let { itemSeleccionado ->
                itemSeleccionado.selectedItemContact( prod )
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_producto, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setData()
        context?.let {
            rv_productos.adapter = adaptador
            rv_productos.layoutManager = LinearLayoutManager(it)
        }

        listener?.renderFirst(first())
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnProductoListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnProductoListener")
        }
    }

    private fun setData() {
        productos.add(Producto(1,"Zanahoria",
            "S/ 30.00","https://www.mundoagro.cl/wp-content/uploads/2014/08/zanahorias_1.jpg"
            ,"La zanahoria es una de las hortalizas más producidas y consumidas en el mundo, Asia es la mayor productora seguida por Europa y los Estados Unidos. De la zanahoria sabemos que beneficia nuestra vista y la salud de nuestra piel, pero su composición rica en vitaminas y minerales reporta otros muchos beneficios para nuestro organismo.")
        )
        productos.add(Producto(2,"Fresa",
            "S/ 10.00","http://www.frutas-hortalizas.com/img/fruites_verdures/presentacio/95.jpg"
            ,"La fresa es un fruto de color rojo brillante, suculento y fragante que se obtiene de la planta que recibe su mismo nombre. En Occidente es considerada la \"reina de las frutas\". Además de poderse comer cruda se puede consumir como compota, mermelada,... Es empleada con fines medicinales ya que posee excelentes propiedades que ayudan a preservar la salud.")
        )

        productos.add(Producto(3,"Plátano",
            "S/ 25.00","http://www.frutas-hortalizas.com/img/fruites_verdures/presentacio/1.jpg"
            ,"El plátano o banano es una fruta amarilla, de forma alargada, que encontramos en el mercado en grupos de tres a veinte, de forma similar a un pepino triangular, oblongo y normalmente de color amarillo. Su sabor es más o menos dulce según la variedad.")
        )
    }

    private fun first(): Producto? {
        productos?.let {
            return it[0]
        }
        return null
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}